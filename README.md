Prova: Desenvolvedor PHP
Empresa: Plannera
======================================

O objetivo da prova é criar um pequeno cadastro de produtos e cálcular a sua importância nas vendas, com um prazo de 48 horas. O sistema foi desenvolvido utilizando uma versão modificada do Yii2 e tive o auxílio de algumas ferramentas como Bootstrap, HighCharts, etc. Alguns dados fictícios foram inseridos, apenas para melhor visualização do projeto, em termos de layout. Para faciliatar a avaliação, subi todos os arquivos do framework, mas isso não é recomendado para ambientes de produção.

======================================
O Schema do banco de dados está no arquivo Plannera.sql e foi utilizado MySQL.
<?php
/* @var $this yii\web\View */
$this->title = 'Prova: Desenvolvedor PHP';
?>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Acompanhamento de vendas'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Qtd. Vendidos'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });
});
		</script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<table id="datatable" style="display: none">
	<thead>
		<tr>
			<th></th>
            <?php
                foreach ($produtos as $produto){
                    echo '<th>' . $produto['name'] . '</th>';
                }
            ?>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th></th>
			<?php
                foreach ($produtos as $produto){
                    echo '<th>' . $produto['sold'] . '</th>';
                }
            ?>
		</tr>
	</tbody>
</table>
<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets_b\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    </head>
</head>
<body>
<?php $this->beginBody() ?>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= Url::toRoute(['/site/']) ?>">Plannera</a> 
            </div>
            <?php
                setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');
                $time = utf8_encode(ucwords(strftime('%A, %d de %B de %Y', strtotime('today'))));
                if (!Yii::$app->user->isGuest){
            ?>
                <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"><?= $time ?> &nbsp; <a href="<?= Url::toRoute(['/site/logout']) ?>" class="btn btn-danger square-btn-adjust" data-method="post">Logout</a> </div>
            <?php
                }
                else{
                    echo '<div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">'. $time . ' &nbsp; <a href="' . Url::toRoute(['/site/login']) . '" class="btn btn-danger square-btn-adjust">Login</a> </div>';
                }
            ?>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
					</li>
				
					
                    <li>
                        <a  href="<?= Url::toRoute(['/site/index']) ?>"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i> Cadastro de Produtos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= Url::toRoute(['/produtos/create']) ?>">Novo Produto</a>
                            </li>
                            <li>
                                <a href="<?= Url::toRoute(['/produtos/']) ?>">Lista de produtos</a>
                            </li>                        
                        </ul>
                      </li>  
                    <li>
                        <a  href="<?= Url::toRoute(['/statistics/view', 'id' => 1]) ?>"><i class="fa fa-bar-chart-o fa-3x"></i> Estatística do Mês Seguinte</a>
                    </li>
                  <!--li>
                        <a class="active-menu"  href="blank.html"><i class="fa fa-square-o fa-3x"></i>Deslogar</a>
                    </li-->	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div >
                    <?php
                     echo $content;
                    ?>                 
                 <hr />
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- METISMENU SCRIPTS -->
        
<?php $this->endBody() ?>   
</body>
</html>
<?php $this->endPage() ?>

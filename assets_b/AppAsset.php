<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets_b;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/assets_b';
    public $css = [
        'css/site.css',
        'css/bootstrap.css',
        'css/custom.css',
        'css/font-awesome.css',
        'js/morris/morris-0.4.3.min.css'
    ];
    public $js = [
        'js/jquery-1.10.2.js',
        'js/bootstrap.min.js',
        'js/jquery.metisMenu.js',
        'js/custom.js',
        'js/highcharts.js',
        'js/modules/data.js',
        'js/modules/exporting.js',
        /*'js/morris/raphael-2.1.0.min.js',
        'js/morris/morris.js'*/
    ];
    public $depends = [
        'yii\web\YiiAsset',
        #'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "produtos".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $sold
 */
class Produtos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'produtos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['sold'], 'integer', 'min' => 0],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Identificador',
            'code' => 'Código do Produto',
            'name' => 'Nome do Produto',
            'sold' => 'Qtd. Vendidos',
        ];
    }
    public function getTotalVendas(){
        $command = Yii::$app->db->createCommand("SELECT sum(sold) FROM produtos");
        $sum = $command->queryScalar();
        return $sum;
    }
}

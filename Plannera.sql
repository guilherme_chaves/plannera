-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25-Mar-2015 às 19:17
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `plannera`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
`id` int(11) NOT NULL COMMENT 'Identificador',
  `code` varchar(255) NOT NULL COMMENT 'Código do Produto',
  `name` varchar(255) NOT NULL COMMENT 'Nome do Produto',
  `sold` int(11) NOT NULL DEFAULT '0' COMMENT 'Qtd. Vendidos'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Tabela para armazenamento dos produtos cadastrados.';

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `code`, `name`, `sold`) VALUES
(3, 'FRUIT001', 'Pêra', 10),
(4, 'FRUIT002', 'Maçã', 50),
(5, 'FRUIT003', 'Melão', 21),
(6, 'FOOD001', 'Feijão Preto', 33),
(7, 'FOOD006', 'Arroz branco', 10),
(8, 'TREASURE001', 'Easter Egg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `statistics`
--

CREATE TABLE IF NOT EXISTS `statistics` (
`id` int(11) NOT NULL,
  `value` float(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `statistics`
--

INSERT INTO `statistics` (`id`, `value`) VALUES
(1, 10.65);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`), ADD KEY `code_2` (`code`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
